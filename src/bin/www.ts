import * as nconf from 'nconf';
nconf.env().file({file: './config.json'});

import {app} from "../app";
import {logger} from "../utils/logger";
import {createServer} from 'http';
import {initSocket} from '../socket/socket'



const port = nconf.get('port') || 2000;
const server = createServer(app.callback());
initSocket(server);
app.on('error', (err: Error) => {
    logger.error(`error: ${err} ${err.stack ? err.stack : ''}`)
});

server.listen(port, () => logger.info(`start listen on port ${port}`));
