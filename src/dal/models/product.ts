import * as mongoose from "mongoose";
import {Schema} from "mongoose";

const productSchema = new Schema<any>({
        name: {type: String, required: true},
        price: {type: Number, required: true},
        amount: {type: Number, required: true},
        description: {type: String, required: true}
    },
    {
        toJSON: {
            transform: function (doc, ret) {
                ret.id = ret._id;
                ret._id = undefined;
                ret.__v = undefined;
                return ret;
            }
        }
    });

export const Product = mongoose.model('product', productSchema);
