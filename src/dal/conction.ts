import * as mongoose from "mongoose";
import {logger} from "../utils/logger";
import * as nconf from 'nconf';

const dbUrl = nconf.get('dbUrl');

export const connectToDb = () => {
    mongoose.connect(dbUrl, {useNewUrlParser: true})
};

mongoose.set('useFindAndModify', false);

mongoose.connection.on('connected', function () {
    logger.info(`Mongoose connection is open to ${dbUrl}`);
});

mongoose.connection.on('error', function (err) {
    logger.info(`Mongoose connection has error ${err} `);
});

mongoose.connection.on('disconnected', function () {
    logger.info("Mongoose connection is disconnected");
});

process.on('exit', function () {
    mongoose.connection.close();
});
