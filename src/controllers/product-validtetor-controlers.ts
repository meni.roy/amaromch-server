import * as Joi from '@hapi/joi';
import {Context} from "koa";

const schema = Joi.object().keys({
    name: Joi.string().alphanum().min(3).max(30),
    price: Joi.number().min(0),
    amount: Joi.number().integer().min(0),
    description: Joi.string(),
});


export const newProductValidatorController = (ctx: Context, next: () => void) => {
    const result = Joi.validate(ctx.request.body, schema, {abortEarly: false, presence: 'required'});
    ctx.validBody = result.value;
    return result.error ? ctx.throw(400, result.error) : next();
};

export const updateProductValidatorController = (ctx: Context, next: () => void) => {
    const result = Joi.validate(ctx.request.body, schema, {abortEarly: false});
    ctx.validBody = result.value;
    return result.error ? ctx.throw(400, result.error) : next();
};
