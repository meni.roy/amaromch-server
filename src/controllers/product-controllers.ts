import {Context} from "koa";
import {Product} from "../dal/models/product";
import {sendBroadcast} from "../socket/socket";
import {socketMassage} from "../socket/socket-massage";
import {ProductData} from "../models/productData";


export const allProductsController = async (ctx: Context, next: () => void) => {
    ctx.body = await Product.find({});
    return next();
};

export const productParamController = async (id: string, ctx: Context, next: () => void) => {
    try {
        const product = await Product.findById(id);
        if (product) {
            ctx.product = product;
            return next();
        } else {
            ctx.throw(404, 'cannot find product')
        }
    } catch (e) {
        e.kind === 'ObjectId' ? ctx.throw(400, 'id isn\'t valid') : ctx.throw(e);
    }
};

export const getProductController = (ctx: Context, next: () => void) => {
    ctx.body = ctx.product;
    return next();
};

export const updateProductController = async (ctx: Context, next: () => void) => {
    const updatedProduct = await Product.findByIdAndUpdate(ctx.product.id, {"$set": ctx.validBody}, {new: true});
    ctx.body = updatedProduct;
    sendBroadcast(socketMassage.productUpdated, updatedProduct as ProductData);
    return next();
};

export const newProductController = async (ctx: Context, next: () => void) => {
    const productProps = ctx.validBody;
    const newProduct = new Product(productProps);
    ctx.body = await newProduct.save();
    sendBroadcast(socketMassage.productCreated, newProduct as ProductData);
    return next();
};

export const delProductController = async (ctx: Context, next: () => void) => {
    await ctx.product.remove();
    sendBroadcast(socketMassage.productDeleted, {id: ctx.product.id});
    ctx.status = 204;
    return next();
};
