import * as Koa from 'koa';
import * as json from 'koa-json';
import {loggerMiddleware} from "./middlewares/logger";
import {logger} from "./utils/logger";
import {router} from "./routes";
import {connectToDb} from './dal/conction'
import errorMiddleware from './middlewares/error-middleware'
import * as koaBody from 'koa-body'

connectToDb();


export const app = new Koa();
app.use(json());
app.use(koaBody());
app.use(loggerMiddleware(logger));
app.use(errorMiddleware);

app.use(router.routes()).use(router.allowedMethods());



