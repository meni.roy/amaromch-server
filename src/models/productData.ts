export interface ProductData {
    id:string,
    name?:string,
    amount?:number,
    description?:string,
    price?:number
}
