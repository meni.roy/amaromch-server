import * as winston from "winston";
import * as nconf from 'nconf';

winston.addColors({
    error: 'red',
    warn: 'yellow',
    info: 'cyan',
    debug: 'green'
});
export const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({filename: nconf.get('errorFile'), level: 'error'}),
        new winston.transports.File({filename: nconf.get('logFile')})
    ]
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.combine(winston.format.colorize({all: true}), winston.format.simple()),
        level:'debug'
    }));
}
