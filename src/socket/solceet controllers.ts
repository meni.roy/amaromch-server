import {ProductData} from "../models/productData";
import {logger} from "../utils/logger";
import {socketMassage} from "./socket-massage";
import * as Joi from '@hapi/joi';
import * as nconf from 'nconf';

export const onDecAmount = connection => (productData: ProductData) => {
    const schema = {id: Joi.string().required(), amount: Joi.number().min(0).required()};
    const {error, value} = Joi.validate(productData, schema);
    if (error) {
        logger.debug(`client with id:${connection.id} send dec amount event. the params not valid error:${error}`);
        connection.emit(socketMassage.error, {status: 400, error: nconf.get('production') ? 'bad request' : error});
        return;
    }
    logger.debug(`client with id:${connection.id} send dec amount event. product id:${value.id} new amount:${value.amount}`);
    connection.broadcast.emit(socketMassage.productUpdated, value);
};
