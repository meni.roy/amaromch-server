import * as io from 'socket.io';
import {Socket} from 'socket.io';
import {Server} from "http";
import {socketMassage} from "./socket-massage";
import {ProductData} from "../models/productData";
import {logger} from "../utils/logger";
import {onDecAmount} from "./solceet controllers";


let socket;
let isInit = false;
export const initSocket = (server: Server) => {
    isInit = true;
    socket = io(server);
    socket.on('connection', (connection: Socket) => {
        logger.info(`io connection open to nsp ${connection.nsp.name} with id: ${connection.id}`);
        connection.on(socketMassage.decAmount, onDecAmount(connection))
    });

    socket.on('disconnect', (connection: Socket) => {
        logger.debug(`io connection close to nsp ${connection.nsp.name} client id: ${connection.id}`)
    });
};

export const sendBroadcast = (massage: socketMassage, value: ProductData) => {
    if (!isInit) {
        throw new Error('socket is not init please init the socket')
    }
    logger.debug(`send broadcast massage:${massage} with data:${JSON.stringify(value)}`);
    logger.info(`send broadcast massage:${massage} `);
    socket.emit(massage, value)
};
