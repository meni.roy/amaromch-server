export enum socketMassage {
    productCreated = 'productCreated',
    productDeleted = 'productDeleted',
    productUpdated = 'productUpdated',
    decAmount = 'decAmount',
    error = 'errorMassage'
}
