import * as Router from 'koa-router';
import {
    allProductsController,
    delProductController,
    getProductController,
    newProductController,
    productParamController,
    updateProductController
} from "./controllers/product-controllers";
import {
    newProductValidatorController,
    updateProductValidatorController
} from "./controllers/product-validtetor-controlers";

export const router = new Router();

router.get('/products', allProductsController);

router.param('product', productParamController);

router.get('/products/:product', getProductController);

router.put('/products/:product', updateProductValidatorController, updateProductController);

router.post('/products', newProductValidatorController, newProductController);

router.delete('/products/:product', delProductController);

