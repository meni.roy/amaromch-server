import {Context} from "koa";
import * as nconf from 'nconf';
import {logger} from "../utils/logger";

export default async (ctx: Context, next: () => void) => {
    try {
        await next();
    } catch (err) {
        if (err.status && err.status < 500) {
            ctx.status = err.status;
            ctx.body = nconf.get('production') ? 'bad request' : {error: err.message};
            logger.warn(
                `user request to ${ctx.method} ${ctx.url} with body params:${JSON.stringify(ctx.request.body)} respond with status :${err.status} error message:${err.message} stack:${err.stack}`)
        } else {
            ctx.status = 500;
            ctx.body = "internal server error";
            ctx.app.emit('error', err);
        }
    }
}
