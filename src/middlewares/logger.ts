import {Context} from "koa";

export const loggerMiddleware = (logger) => async (ctx: Context, next: () => void) => {
    const start = new Date().getTime();
    logger.info(`got ${ctx.method}' to '${ctx.url}' body params:${JSON.stringify(ctx.request.body)}`);
    await next();
    const ms = new Date().getTime() - start;
    logger[ctx.status === 500 ?'error':'info']
    (`request '${ctx.method}' to '${ctx.url}' body params:${JSON.stringify(ctx.request.body)} took:'${ms}' ms respond with status:'${ctx.status}' `)
};
